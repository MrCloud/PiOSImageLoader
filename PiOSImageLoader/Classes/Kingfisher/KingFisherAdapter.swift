//
//  KingFisherAdapter.swift
//  Pods
//
//  Created by Florian PETIT on 24/02/2017.
//
//

import Kingfisher

class KingFisherAdapter: ImageLoaderAdapter {
    
    func loadImage(url: URL, imageView: UIImageView) {
        imageView.kf.setImage(with: url)
    }
    
}
