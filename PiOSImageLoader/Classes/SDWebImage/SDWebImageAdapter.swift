//
//  KingFisherAdapter.swift
//  Pods
//
//  Created by Florian PETIT on 24/02/2017.
//
//

import SDWebImage

class SDWebImageAdapter: ImageLoaderAdapter {
    
    func loadImage(url: URL, imageView: UIImageView) {
        imageView.sd_setImage(with: url)
    }
    
}
