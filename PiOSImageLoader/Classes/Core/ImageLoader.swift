//
//  ImageLoader.swift
//  Pods
//
//  Created by Florian PETIT on 24/02/2017.
//
//

import UIKit

/**
 ImageLoaderAdapter protocol
 */
protocol ImageLoaderAdapter {
    /**
     load an image
     - parameter url:           the URL of the image to be loaded
     - parameter imageView:     the imageView that should contain the image loaded
     */
    func loadImage(url: URL, imageView: UIImageView)
}

/**
A class used to load images asynchronously
 */
public class ImageLoader: NSObject {
    /// An adapter used to load the image
        var imageLoaderAdapter: ImageLoaderAdapter?
    
    /**
     Initializes an imageLoader according the selected implementation
     */
    public override init() {
        #if PiOSKingfisher
            imageLoaderAdapter = KingFisherAdapter()
        #endif
        #if PiOSNuke
            imageLoaderAdapter = NukeAdapter()
        #endif
        #if PiOSSDWebImage
            imageLoaderAdapter = SDWebImageAdapter()
        #endif
    }
    
    
    /**
     Load an image with the configured adapter
     - parameter url:           the URL of the image to be loaded
     - parameter imageView:     the imageView that should contain the image loaded
     */
   public func loadImage(url: URL, imageView: UIImageView) {
        self.imageLoaderAdapter?.loadImage(url: url, imageView: imageView)
    }
}
