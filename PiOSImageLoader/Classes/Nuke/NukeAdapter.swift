//
//  NukeAdapter.swift
//  Pods
//
//  Created by Florian PETIT on 24/02/2017.
//
//

import Nuke

class NukeAdapter: ImageLoaderAdapter {
    
    func loadImage(url: URL, imageView: UIImageView) {
        Nuke.loadImage(with: url, into: imageView)
    }
    
}
