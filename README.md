PiOSImageLoader
============

PiOSImageLoader is a wrapper for asynchronous image-loading libraries for iOS.

- [Features](#features)
- [The Basics](#the-basics)
- [To Do](#to-do)
- [Contributing](#contributing)
- [Installation](#installation)

# Features:

# The Basics
PiOSImageLoader helps you use any of the provided implementation of asynchronous image-loading and quickly change the chosen implementation by updating the podfile used in your project, thus this projectis highly dependant on CocoaPods.


```swift
// Simple usage whatever the provided implementation you choose
let imageView = UIImageView()
let url = URL(string: "http://www.example.com")!
ImageLoader().loadImage(url: url, imageView: imageView)
```

In order to switch between the provided libraries you just need to update the podfile:

for example from:
```ruby
pod 'PiOSImageLoader/KingFisher', '~> 0.9'
```

to:
```ruby
pod 'PiOSImageLoader/SDWebImage', '~> 0.9'
```

# To Do
- Support more image-loading APIs methods, with more options, cache policies...
-

# Contributing

Contributions are very welcome 👍😃.

Before submitting any pull request, please ensure you have run the included tests (if any) and they have passed. If you are including new functionality, please write test cases for it as well.

# Installation
### Cocoapods
PiOSImageLoader can be added to your project using [CocoaPods](http://cocoapods.org) by adding the following lines to your `Podfile`:

```ruby
source 'https://gitlab.com/MrCloud/PiOSPodSpecs'

pod 'PiOSImageLoader/KingFisher', '~> 0.9'
```
